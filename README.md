# Public E-Appointment

Public E-Appointment is a software for online booking of appointments and processing of queues such as calling appointment numbers and collecting statistics on services provided.

The software has been used in public administration in the German capital Berlin for more than 20 years and has been redeveloped under a new license since 2016. This allows the software to be re-released under the EUPL, an open source license recognized by OSI.

It is planned to release the software as open source in the course of 2022/2024. This requires a number of adjustments, so that step by step the individual components of the software will be published here.

On the one hand, the documentation of the software is published in this repository, on the other hand, new ideas and further developments are planned here, which apply across the board for the other repositories.

## Contribution

See [CONTRIBUTION.md](CONTRIBUTION.md) for information about bug reports, new issues, coding conventions and how to run tests.

Refer to [docs/](./docs/index.md) for more technical documents.

## Overview Repositories

The project has a lot of repositories. For a overview, here is a short description for each one:

| Description | Repository |Status|
| --- | --- | --- |
|This repository you are currently viewing contains documentation and is the primary source for issues| eappointment|
|User handbook in docsphinx format|zmsmanual |(planned release 2024)|
|**Applications**|
|This JSON based HTTP API is the core of the service oriented architecture|[zmsapi](https://gitlab.com/eappointment/zmsapi/) |[![pipeline status](https://gitlab.com/eappointment/zmsapi/badges/main/pipeline.svg)](https://gitlab.com/eappointment/zmsapi/) [![coverage report](https://gitlab.com/eappointment/zmsapi/badges/main/coverage.svg)](https://eappointment.gitlab.io/zmsapi/_tests/coverage/index.html)|
|A small JSON based HTTP API proxy to realize access based on quotas|zmsapiproxy |(planned release 2024)|
|A small agent to send mails and notifications|[zmsmessaging](https://gitlab.com/eappointment/zmsmessaging) |[![pipeline status](https://gitlab.com/eappointment/zmsmessaging/badges/main/pipeline.svg)](https://gitlab.com/eappointment/zmsmessaging/) [![coverage report](https://gitlab.com/eappointment/zmsmessaging/badges/main/coverage.svg)](https://eappointment.gitlab.io/zmsmessaging/_tests/coverage/index.html)|
|Administration interface and main workspace for public agents|[zmsadmin](https://gitlab.com/eappointment/zmsadmin/) |[![pipeline status](https://gitlab.com/eappointment/zmsadmin/badges/main/pipeline.svg)](https://gitlab.com/eappointment/zmsadmin/) [![coverage report](https://gitlab.com/eappointment/zmsadmin/badges/main/coverage.svg)](https://eappointment.gitlab.io/zmsadmin/_tests/coverage/index.html)|
|Interface to obtain usage statistics|[zmsstatistic](https://gitlab.com/eappointment/zmsstatistic/) |[![pipeline status](https://gitlab.com/eappointment/zmsstatistic/badges/main/pipeline.svg)](https://gitlab.com/eappointment/zmsstatistic/) [![coverage report](https://gitlab.com/eappointment/zmsstatistic/badges/main/coverage.svg)](https://eappointment.gitlab.io/zmsstatistic/_tests/coverage/index.html)|
|A calldisplay for calling clients to a room or a place|[zmscalldisplay](https://gitlab.com/eappointment/zmscalldisplay/) |[![pipeline status](https://gitlab.com/eappointment/zmscalldisplay/badges/main/pipeline.svg)](https://gitlab.com/eappointment/zmscalldisplay/) [![coverage report](https://gitlab.com/eappointment/zmscalldisplay/badges/main/coverage.svg)](https://eappointment.gitlab.io/zmscalldisplay/_tests/coverage/index.html)|
|A web interface for calling clients to a room or a place|zmswebcalldisplay |(planned release 2024)|
|An interface to get wait numbers for spontaneous customers|[zmsticketprinter](https://gitlab.com/eappointment/zmsticketprinter/) |[![pipeline status](https://gitlab.com/eappointment/zmsticketprinter/badges/main/pipeline.svg)](https://gitlab.com/eappointment/zmsticketprinter/) [![coverage report](https://gitlab.com/eappointment/zmsticketprinter/badges/main/coverage.svg)](https://eappointment.gitlab.io/zmsticketprinter/_tests/coverage/index.html)|
|Interface for a callcenter|d115mandant |(planned release 2024)|
|**Libraries**|
|A small validation library for user input|[mellon](https://gitlab.com/eappointment/mellon/) |[![pipeline status](https://gitlab.com/eappointment/mellon/badges/master/pipeline.svg)](https://gitlab.com/eappointment/mellon/) [![coverage report](https://gitlab.com/eappointment/mellon/badges/master/coverage.svg)](https://gitlab.com/eappointment/mellon/)|
|JSON Schema for entities and PHP classes|[zmsentities](https://gitlab.com/eappointment/zmsentities/) |[![pipeline status](https://gitlab.com/eappointment/zmsentities/badges/main/pipeline.svg)](https://gitlab.com/eappointment/zmsentities/) [![coverage report](https://gitlab.com/eappointment/zmsentities/badges/main/coverage.svg)](https://eappointment.gitlab.io/zmsentities/_tests/coverage/index.html)|
|A PHP client library to access the HTTP API|[zmsclient](https://gitlab.com/eappointment/zmsclient/) |[![pipeline status](https://gitlab.com/eappointment/zmsclient/badges/main/pipeline.svg)](https://gitlab.com/eappointment/zmsclient/) [![coverage report](https://gitlab.com/eappointment/zmsclient/badges/main/coverage.svg)](https://eappointment.gitlab.io/zmsclient/_tests/coverage/index.html)|
|A PHP library to access the MySQL database schema|[zmsdb](https://gitlab.com/eappointment/zmsdb/) |[![pipeline status](https://gitlab.com/eappointment/zmsdb/badges/main/pipeline.svg)](https://gitlab.com/eappointment/zmsdb/) [![coverage report](https://gitlab.com/eappointment/zmsdb/badges/main/coverage.svg)](https://eappointment.gitlab.io/zmsdb/_tests/coverage/index.html)|
|Base library for using the PHP slim framework|[zmsslim](https://gitlab.com/eappointment/zmsslim/) |[![pipeline status](https://gitlab.com/eappointment/zmsslim/badges/main/pipeline.svg)](https://gitlab.com/eappointment/zmsslim/) [![coverage report](https://gitlab.com/eappointment/zmsslim/badges/main/coverage.svg)](https://eappointment.gitlab.io/zmsslim/_tests/coverage/index.html)|
|Library to import and access required service and location data|[zmsdldb](https://gitlab.com/eappointment/zmsdldb/) |[![pipeline status](https://gitlab.com/eappointment/zmsdldb/badges/main/pipeline.svg)](https://gitlab.com/eappointment/zmsdldb/) [![coverage report](https://gitlab.com/eappointment/zmsdldb/badges/main/coverage.svg)](https://eappointment.gitlab.io/zmsdldb/_tests/coverage/index.html)| 
|CSS and layout examples for the backend user interface|[layout-admin-scss](https://gitlab.com/eappointment/includes/layout-admin-scss/)|-|
|JS for layout examples for the backend user interface|[layout-admin-js](https://gitlab.com/eappointment/includes/layout-admin-js/)|-|

## Security issues

See [Security.md](Security.md).