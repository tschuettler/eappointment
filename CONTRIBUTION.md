Thank you first, for considering contributing to this project. We welcome any contribution in any form. To make the process as smooth as possible, we have created the following guidelines. 

# Table of Contents

* Documentation links
* Submit changes
* Coding conventions

# Documentation links

The file [README.md](README.md) contains a first insight about the project and contains all relevant links to documentation.

# Submit bugs

The central repository for this project resides under https://gitlab.com/eappointment/eappointment/-/issues

Please use the provided template for bugs. Try to check, if the bug is already present in the issue list.

If you have an internal ticket system, it is allowed to add a link to your system at the bottom of the description.

# Submit changes

The central repository for this project resides under https://gitlab.com/eappointment/eappointment/

To submit a change, please add a merge request with your changes.

# GIT conventions

## Branches

### **Naming scheme**

- Each organization has a namespace, for example an abbreviation like "muc" or "berlin". Shared branches do not have a namespace/abbreviation.

- Feature branches are named the same for a given feature in all participating repositories.

- Branch names are separated with a "-" (/,\ or # will produce errors in some cases) . For compatibility reasons, there must not be a branch "berlin-release" if "berlin-release-1.2.3" is to be created.


### **Branch examples**

*abbrev*-feature-*issue or name* (muc-feature-oidc)

*abbrev*-hotfix-*issue or name* (berlin-hotfix-24)

*abbrev*-bug-*issue or name* (berlin-bug-52889)


### **Merge Requests**

Merge/pull requests should always go to "main" by default. The branch is only deleted when it has been merged with "main".

- release branches should branch directly from "main". If an old release branch is used that is not yet in "main", a rebase should take place at the beginning.
- after a release in the "main" branch, a rebase should be performed on unmerged branches.
- dependencies in composer.json must always be specified as minimum version when merging in "main".

**naming examples**

organization-specific feature: "[abbrev] feature: feature name"

organization-specific bug: "[abbrev] bug: bugname"

organization-specific hotfix: "[abbrev] hotfix: hotfixname"

cross-cutting feature: "[main] feature: feature name"

cross-cutting bug: "[main] bug: bugname"

cross-cutting hotfix: "[main] hotfix: hotfixname"

---
## Release names

With regard to the naming of a release, a standard has been established in recent years as to how to assign a version number. This standard is called ["semantic versioning"](http://semver.org/). 

We name the releases with a version number, which consists of three numbers that have the following meaning:

    MAJOR.MINOR.PATCH

The numbers have the following meaning:

* **PATCH**: This number is always incremented by one if no features are added or removed with the changes and no incompatibilities occur. Usually this is a release that only fixes bugs.
* **MINOR**: This number is incremented by one whenever the scope of features changes. However, no incompatibilities occur.
* **MAJOR**: This number is incremented by one whenever breaking changes with the previous version occur.

If the rules behind the numbering of software releases are known, it is already possible to see from the version number what effects the release might have. The numbers are not limited to the digits "0" to "9". A version like "2.31.2" is also conceivable. Especially the first number will only be increased in exceptional cases.


# Testing

For every application a defined test coverage is mandatory. The measurement of test coverage is line coverage. 

| Repositories | Minimal line coverage |
| --- | --- |
| zmsentities, zmsapi, zmsdb, zmsadmin, zmscalldisplay, zmsticketprinter, zmsstatistic | > 90% |
| zmsdldb, zmsclient, zmsslim| > 80% |


## General requirements

Under `bin/test` is a shell script to run all tests on a repository. If new tests are added, this script has to be modified.

Some general rules should be followed:

* Do not use any personal data in the tests, no names, telephone numbers or adresses
* Examples email adresses should end on `@example.com`

## Syntax checks

Every PHP-Repository needs an PHP-Codesniffer test. This test checks the PSR-2 rules. Usually the test is run in the `bin/test` script like `vendor/bin/phpcs --standard=psr2`

##  Metrics checks

Every repository has to comply to defined metrics. For PHP the tool PHPMD (PHP Mess Detector) is used. Every repository contains a `phpmd.rules.xml` in the root directory. To run only the phpmd tests, use `vendor/bin/phpmd src/ text phpmd.rules.xml`

## Unit and behavior tests

For testing PHP line coverage, we use the framework PHPUnit. Every repository has to contain a `phpunit.xml` in the root directory. The tests are located in the path `tests/RepoName` and fixtures are located in the path `tests/RepoName/fixtures/`.

To run the tests use `vendor/bin/phpunit`.

To generate code coverage reports locally, use `php -dzend_extension=xdebug.so -dmemory_limit=-1 vendor/bin/phpunit -v --coverage-text --coverage-html public/_tests/coverage/ `

Automatically generated coverage reports are linked in the [Readme](README.md).

Usually a `Base` class ensures valid test data and should be used on every tests. Every test is executed in a database transaction. After every test, a rollback is initiated to ensure the same test data.


### Unit tests in libraries

Unit tests do not require any helper classes for basic testing. We use some libraries, for example for mocking classes. 

* For testing HTTP requests, we use the [MMock](https://github.com/jmartin82/mmock). An example can be found [here](https://gitlab.com/eappointment/zmsclient) (take a look into the `.gitlab-ci.yml` and the `docker-compose.yml` file for starting the mockup server).
* As PHP object mocking framework we use [prophecy](https://github.com/phpspec/prophecy), an example can be found [here](https://gitlab.com/eappointment/zmsclient/-/blob/main/src/Zmsclient/PhpUnit/Base.php)


### Behavior tests with controllers

If the base library `zmsslim` is used, the PHPUnit-Tests extend [\BO\Slim\PhpUnit\Base](https://gitlab.com/eappointment/zmsslim/-/blob/main/src/Slim/PhpUnit/Base.php). These tests usually cover the behavior of a controller. A few helper functions are used:

* Define a `$classname` for the controller.
* Entities from `zmsentities` usually have a function `createExample()` for a quick example.
* Define at least a function `testRendering()` to ensure basic rendering, further tests can have a name like `testMyTest()`. If the `testRendering()` is not defined, a basic render test is used, which checks, if a 200 http status code is returned.
* Use `$this->render($arguments = [], $parameters = [], $sessionData = null, $method = 'GET')`.

```php
namespace BO\Zmsapi\Tests;

use \BO\Zmsentities\MyEntitiy as Entity;

class MyControllerTest extends Base
{
    protected $classname = "MyController";

    public function testRendering()
    {
        $input = (new Entity)->createExample();
        $response = $this->render(['key' => 'mykey', '__header' => ['X-Authkey'=>'example'], '__body'=>json_encode($input)], [], [], 'POST');
        $this->assertStringContainsString('example response', (string)$response->getBody());
        $this->assertTrue(200 == $response->getStatusCode());
    }
}
```


### Unit tests in zms client applications

For testing API requests, we use the [\BO\Zmsclient\PhpUnit\Base](https://gitlab.com/eappointment/zmsclient/-/blob/main/src/Zmsclient/PhpUnit/Base.php) class. This class extends [\BO\Slim\PhpUnit\Base](https://gitlab.com/eappointment/zmsslim/-/blob/main/src/Slim/PhpUnit/Base.php) for testing controllers. It is used to get defined responses for API calls:

```php

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'response' => $this->readFixture("GET_Workstation_UserAccountMissingLogin.json")
                ]
            ]
        );
        $response = $this->render($this->arguments, [], []);
        $this->assertStringContainsString('Anmeldung', (string)$response->getBody());
        $this->assertEquals(200, $response->getStatusCode());
    }

```

# Coding conventions

Due to automated tests or a code review of a merge requests, the following rules might require changes. 

There may always be reasons not to follow the rules. In that case, a comment with a short explanation should be added to the code.

These rules are there to implement a code style that is as consistent as possible. No claim is made that these are the best possible rules, but for collaborating on a project together we had decided to use common rules.

## Code metrics

For our tests we have chosen the tool PHPMD. It is important for us that the following metrics are supported. The values refer to the used language PHP, for other languages the values can vary slightly.

**Cyclomatic complexity**: The metric named after McCabe is one of the best known. It measures the complexity of a function. The more complex the function, the harder it is to understand. A possible error is harder to find. The limit value here is 11.

**Method length**: The longer a function, the less clear the code is. The limit value is 100 lines. However, the value will rarely be achievable due to the cyclomatic complexity.

**Number of parameters**: As a rule, methods should never have more than three parameters. Only in a few exceptional cases this can make sense. The limit value here is 10.

**Superglobals**: This rule is PHP specific and prohibits the direct use of external input without validation.

**Eval**: In scripting languages like PHP there is the possibility to execute code with eval(). This should not occur.

**Goto**: The use of GOTO statements, if possible, is not allowed.

**Number of Children**: In object orientation there should not be too many inheritances of a class. The limit for this is 15.

**Depth of Inheritance**: If the chain of inheritance is too long, the code becomes more difficult to read. The limit for this is 6.

**Coupling between Objects**: Dependencies to other classes should be kept low. The limit here is 13.

**Variable length**: The use of incorrectly named or poorly named variables noticeably lowers the readability of code. The minimum length of a variable is 3 characters and the maximum is 20 characters.

**Unused variables**: If a variable is not used, it can be deleted. Such a test allows the detection of incorrectly written variables and unintended null values in scripting languages.

**Method name**: A method should always reflect exactly what it does. In addition, a method should always represent only a single function. At least 3 characters are required. See also the naming rules for code.

Another important requirement for the readability of the source code is a uniform use of the spelling of the code. For this there are clear rules for PHP, which were defined in the **PSR2 standard**. By means of a so-called "code sniffer" this will be checked automatically.


## Naming rules for code

We like to archive code documentation by meaningful naming of classes, functions and properties. 

**Comprehensibility**: A clearly understandable name should rather be chosen than a short one.

*Example*: `location()` to get a location should better be called `getLocation()`. If the functions uses something else than the common primary key, it is better to use a name like `getLocationByLeika()`.

**No abbreviations**: No abbreviations may be used in the names. Exceptions are official abbreviations such as ``Leika''.

*Example*: Instead of `getLoc()` use `getLocation()`, instead of `$c` use `$counter`.

**Clear name**: The name should be unique and does not allow multiple interpretations.

*Example*: Instead of `displayName()`, either `getDisplayname()` or `printf(getName())` should be used, depending on usage.

**Consistency**: names and labels should be used consistently in the code.

*Example*: Instead of `$serviceId`, `$service` or `$id`, `$serviceId` should always be used.

**Hungarian notation**: the type of the content of a variable should be recognizable.

*Example*: Instead of `$time`, `$timeInDays` or `$timeInSeconds`, better still `$elapsedSeconds` or `$elapsedDays` should be used.

**Return values**: Instead of relying on the documentation, the name of a function should allow conclusions about the returned values (if not self or void).

*Example*: A `getLocation()` should return an object, the id should be available via `getLocationId()`. A `getLocationWithAppointmentsArray()` should return an array of locations offering appointment services.

**Informative**: The purpose of variables should be clearly stated. Numbering or placeholders should be avoided.

*Example*: `$customer2` should become either a `$clonedCustomer` or a `$customerWithAddress`.

**Boolean**: Variables or methods that contain or return true or false should be named in the form of a question.

*Example*: Instead of `$enabled` or `appointments()` use `$isEnabled` or `hasAppointments()`.

**Unique**: Each variable should appear only once and should not appear more than once in different contexts.

*Example*: Instead of a common variable like `$temp` it is better to use `$lastAppointment`.

### Possible naming prefixes

Somes prefixes are always reused. Names of public methods in classes must always use a prefix. Sometimes, however, different prefixes can be found with the same meaning, such as "fetch" and "read". The following is a list of common prefixes in this project and their meaning:

| prefix | description | replaced prefixes|
| --- | --- | --- |
|add| Add something to a list.| (Replaces "push", "append", "prepend", "unshift")|
|close| Closes a connection. |(Replaces "kill", "clear", "destroy", "end")|
|create| Creates an object, without name addition a static "constructor". Often used as a static method in the form "createFrom".| (Replaces "from", "build", "compile", "configure", "evaluate", "generate", "init", "merge", "parse")|
|delete| Like "remove" but with global side effects| (Replaces "unlink", "erase", "kill", "drop")|
|get| Getter for returning or creating an existing value.| (Replaces "calculate", "compute", "constant", "count", "current", "data", "find", "provide", "query", "resolve", "search")|
|has| (Boolean) Tests whether a list has a particular member.| (Replaces "contains", "exists")|
|is| (Boolean) Checks, checks for a specific property. |(Replaces "bool", "enabled", "disabled", "check", "expect", "matches", "should")|
|open| Opens a connection for reading and writing with global side effects.| (Replaces "connect", "exec").|
|read| Same as "get", but this variable indicates that global side effects such as IO operations are necessary and care should be taken when using it. |(Replaces "fetch", "retrieve", "request", "load", "search")|
|remove| Remove a value. |(Replaces "pop", "shift")|
|restore| Restore a (default) value. |(Replaces "clear", "reset")|
|sanitize| Modifies a value for further use. |(Replaces "escape", "filter", "format")|
|set| Setter for setting a value |(Replaces "be", "allow", "deny", "enable", "expect", "disable", "from", "on", "parse", "register", "update", "will")|
|start| Starts a process with possible global side effects, should only be used as prefix and not alone. |(Replaces: "do", "work", "make", "process", "run")|
|test| (Exception) Tests a property and stops the program if an error occurs. |(Replaces "assert", "check", "expect")|
|to| Transforms or interprets an object to something else. Example: `Object::toFormat()` or `Helper::toFormat($fromFormat)` |(Replaces "transform", "analyze", "build", "copy", "filter", "export", "format", "render", "sort") |
|validate| (Boolean) Checks whether a value is valid. |(replaces "analyze", "approve")|
|with| (clone) Used like "set" but results in a clones objects to avoid side effects| (keyword "immutability")|
|write| Like "set" but with global side effects. |(Replaces "save", "print", "echo", "display", "dump", "query", "send")|

Unwanted prefixes: "if", "not".

## Further coding guidelines

**return values**: A function should always return what it changes. Since return allows only one value, functions should never change more than one thing. Therefore, a getter never changes anything about the object. A setter should return its object.

**Side effects**: Global variables or even so-called "manager" or "context" classes, which are initialized as a property of almost all objects, should be avoided. If such global variables are used, they should be used read-only. An exception is the configuration for initialization.

**Exceptions**: Exceptions should terminate the program. The use to control a program (control flow) should be avoided. The exception is an external library that requires this control.

**Readability before convenience**: Magic methods and hiding important functions should be avoided. It should always be possible to follow the path of execution without using a debugger or a sophisticated IDE.
