# Entity Relationship Models

This entity relationship diagram shows the relationships between the entities. For entity definitions, see [the repository zmsentities (JSON-Schema)](https://gitlab.com/eappointment/zmsentities/-/blob/main/schema/)

For relationship between database tables, see [db-erm.md](db-erm.md).


```mermaid
erDiagram

    OWNER

    APICLIENT {
        string glossar "Public API User"
    }
    APIKEY {
        string glossar "Public API Secret Key"
    }
    APPOINTMENT {
        string glossar "Terminzeit"
    }
    AVAILABILITY {
        string glossar "Öffnungszeit"
    }
    CALENDAR {
        string glossar "Terminsuche"
    }
    CALLDISPLAY {
        string glossar "Aufrufbildschirm"
    }
    CLIENT {
        string glossar "Terminkunde"
    }
    CLUSTER {
        string glossar "Cluster von Standortkalendern"
    }
    CONFIG {
        string glossar "Konfigurationseinstellungen"
    }
    CONTACT {
        string glossar "Kontaktadresse"
    }
    DAY {
        string glossar "Tag mit Terminen"
    }
    DAYOFF {
        string glossar "Freie Tage/Feiertag"
    }
    %%DEPARTMENT
    EXCHANGE  {
        string glossar "Datendefinition für Statistik-Export"
    }
    ICS {
        string glossar "ICS Attachment for E-Mail"
    }
    LINK {
        string glossar "Quick-Links für Ansicht Sachbearbeitung"
    }
    LOG {
        string glossar "Log-Entry für Events"
    }
    MAIL {
        string glossar "E-Mail"
    }
    METARESULT {
        string glossar "Envelope für API-Antworten"
    }
    MIMEPART {
        string glossar "Teile/Anhänge der E-Mail"
    }
    MONTH {
        string glossar "Tage gruppiert nach Monat"
    }
    NOTIFICATION {
        string glossar "SMS"
    }
    %%ORGANISATION
    %%OWNER
    PROCESS {
        string glossar "Vorgang mit Termin"
    }
    PROVIDER {
        string glossar "Anbieter für Dienstleistungen"
    }
    QUEUE {
        string glossar "Liste von Vorgängen in einem Standortkalender"
    }
    REQUEST {
        string glossar "Dienstleistung"
    }
    REQUESTRELATION {
        string glossar "Beziehung Dienstleistungen zu Anbietern"
    }
    SCOPE {
        string glossar "Standortkalender"
    }
    SESSION {
        string glossar "Session während Terminbuchung"
    }
    SLOT {
        string glossar "Zeitschlitz für Terminzeit"
    }
    SOURCE {
        string glossar "Quelle für Dienstleistungen/Anbieter"
    }
    STATUS {
        string glossar "Statusinformationen für das System"
    }
    TICKETPRINTER {
        string glossar "Wartenummern für Spontankunden"
    }
    USERACCOUNT {
        string glossar "Nutzerkonto für Login"
    }
    WORKSTATION {
        string glossar "Eingeloggtes Nutzerkonto"
    }

    OWNER ||--o{ ORGANISATION: has
    ORGANISATION ||--o{ DEPARTMENT: has
    DEPARTMENT ||--o{ SCOPE: has

    ORGANISATION ||--o{ TICKETPRINTER: has
    TICKETPRINTER }o--o{ SCOPE: has
    TICKETPRINTER }o--o{ CLUSTER: has

    DEPARTMENT ||--o{ CLUSTER: has
    CLUSTER ||--o{ SCOPE: has

    CALLDISPLAY }o--o{ SCOPE: relation
    CALLDISPLAY }o--o{ CLUSTER: relation
    
    SOURCE ||--o{ PROVIDER: has
    SOURCE ||--o{ REQUEST: has
    SOURCE ||--o{ REQUESTRELATION: has
    PROVIDER ||--o{ SCOPE: has
    REQUESTRELATION }o--o{ PROVIDER: has
    REQUESTRELATION }o--o{ REQUEST: has

    SCOPE ||--o{ QUEUE: relation
    QUEUE ||--o{ PROCESS: has
    SCOPE ||--o{ AVAILABILITY: has

    DEPARTMENT ||--o{ MAIL: "From Header"
    DEPARTMENT ||--o{ DAYOFF: has
    DEPARTMENT ||--o{ USERACCOUNT: has
    DEPARTMENT ||--o{ LINK: has
    USERACCOUNT ||--o{ WORKSTATION: has

    WORKSTATION }o--|| SCOPE: has
    WORKSTATION }o--|| CLUSTER: relation
    WORKSTATION ||--o| PROCESS: processing

    LOG }|--|| PROCESS: relation
    LOG }|--|| AVAILABILITY: relation

    PROCESS ||--|| APPOINTMENT: contains
    PROCESS ||--|| CLIENT: contains
    PROCESS ||--o{ REQUEST: has

    CLIENT ||--|| CONTACT: contains

    DAYOFF }o--o{ AVAILABILITY: exempts
    APPOINTMENT }o--|| AVAILABILITY: matches
    AVAILABILITY ||--o{ SLOT: has
    APPOINTMENT ||--|{ SLOT: has

    PROCESS }o--o{ MAIL: has
    MAIL }o--o{ CLIENT: has
    MAIL ||--o{ MIMEPART: has
    MIMEPART ||--|| ICS: relation

    PROCESS }o--o{ NOTIFICATION: has
    NOTIFICATION }o--o{ CLIENT: has

    CALENDAR ||--|{ SCOPE: matches
    CALENDAR ||--|{ PROVIDER: matches
    CALENDAR ||--|{ REQUEST: matches

    CALENDAR ||--o{ DAY: has
    MONTH ||--o{ DAY: has
    DAY ||--o{ PROCESS: relation

```
