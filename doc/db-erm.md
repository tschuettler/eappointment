# Database table relationship diagram

This diagram shows the realtionships of database tables in the MySQL database. For relationships between entities see [erm.md](erm.md).

Only the columns that are related to other tables are listed. 

* If the fieldname between two tables is not the same, both fieldnames are listed in the connection. Example: A `JOIN ON a.processID=b.BuergerID` is listed as `processID-BuergerID`. 
* Special cases with multiple fields are connected with a double `--`. Example: A `JOIN a.source=b.source AND a.InfoDienstleisterID=b.id` is listed as `source--id-InfoDienstleisterID`.

```mermaid
erDiagram

 kunde {
  int KundenID
 }
 organisation {
  int OrganisationsID
  int KundenID
 }
 behoerde {
  int BehoerdenID
  int OrganisationsID
 }
 standort {
  int StandortID
  int BehordenID
  int InfoDienstleisterID
  string source
 }
 buerger {
  int BuergerID
  int StandortID
 }
 oeffnungszeit {
  int OeffnungszeitID
  int StandortID
 }
 slot {
  int slotID
  int scopeID
  int availabilityID
 }
 slot_process {
  int slotID
  int processID
 }
 buergeranliegen {
  int BuergerID
  int BuergerarchivID
  int AnliegenID
  int source
 }
 provider {
   string source
   string id
 }
 request {
   string source
   string id
 }
 request_provider {
  string source
  string request__id
  string provider__id
 }
 source {
  string source
 }
 apiclient {
  int apiClientID
 }
 apikey {
  int apiClientID
  string key
 }
 apiquota {
  string key
 }
 buergerarchiv {
  int BuergerarchivID
  int StandortID
 }
 abrechnung {
  int StandortID
 }
 standortcluster {
  int clusterID
 }
 clusterzuordnung {
  int clusterID
  int StandortID
 }
 config {
  string name
 }
 email {
  int BehoerdenID
 }
 feiertage {
  int BehordenID
 }
 imagedata {
  string imagename
 }
 kiosk {
  int organisationsid
 }
 kundenlinks {
  int behoerdenid
 }
 log {
  int refrence_id
 }
 notificationqueue {
  int processID
 }
 mailqueue {
  int id
  int processID
  int departmentID
 }
 mailpart {
  int queueId
 }
 nutzer {
  int NutzerID
  int BehoerdenID
  int StandortID
 }
 nutzerzuordnung {
  int nutzerid
  int behoerdenid
 }
 sessiondata {
   string sessionid
 }
 sms {
  int BehoerdenID
 }
 wartenrstatistik {
  int standortid
 }

 kunde ||--o{ organisation: KundenID
 organisation ||--o{ behoerde: OrganisationsID
 behoerde ||--o{ standort: BehoerdenID
 standort |o--o{ buerger: StandortID
 standort ||--o{ oeffnungszeit: StandortID
 oeffnungszeit ||--o{ slot: OeffnungszeitID-availabilityID
 standort ||--o{ slot: StandortID-scopeID
 slot ||--o{ slot_process: slotID
 buerger ||--|| slot_process: BuergerID-processID
 buerger ||--o{ buergeranliegen: BuergerID
 buergeranliegen ||--|| request: source--AnliegenID-id
 standort ||--|| provider: source--InforDienstleisterID-id
 request_provider ||--|| provider: source--id-provider__id
 request_provider ||--|| request: source--id-request__id
 source ||--o{ provider: source
 source ||--o{ request: source
 source ||--o{ request_provider: source
 apiclient ||--o{ buerger: apiClientID
 apiclient ||--o{ apikey: apiClientID
 apikey ||--|| apiquota: key
 standort ||--o{ buergerarchiv: StandortID
 buergeranliegen ||--|| buergerarchiv: BuergerarchivID
 standort ||--o{ abrechnung: StandortID
 standortcluster||--o| clusterzuordnung: clusterID
 standort ||--o| clusterzuordnung: StandortID
 behoerde ||--|| email: BehordenID
 behoerde ||--o{ feiertage: BehordenID
 organisation ||--o{ kiosk: organisationsid-OrganisationsID
 behoerde ||--o{ kundenlinks: behoerdenid-BehoerdenID
 buerger ||--o{ notificationqueue: BuergerID-processID
 buerger ||--o{ mailqueue: BuergerID-processID
 mailqueue ||--o{ mailpart: id-queueID
 behoerde ||--o{ mailqueue: departmentID-BehoerdenID
 behoerde ||--o{ nutzerzuordnung: BehoerdenID-behoerdenid
 nutzerzuordnung ||--|| nutzer: nutzerid-NutzerID
 standort |o--o{ nutzer: StandortID
 nutzer |o--o| buerger: BuergerID
 behoerde ||--|| sms: BehoerdenID
 standort ||--o{ wartenrstatistik: standortid-StandortID
 ```