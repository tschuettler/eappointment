## Lifecycle of the entity "process"

A `process` is a central entity in the application. It connects different entities like `appointment`, `client`, `scope`.

Each `process` has a defined status [described in the entity process (JSON-Schema)](https://gitlab.com/eappointment/zmsentities/-/blob/main/schema/process.json). This diagram shows the life cycle of a `process`-entity with all all status-changes:

### Explanation of canceled -> blocked appointments

A canceled appointment slot (whether by citizens or clerks) is released again after a deletion period defined in the location. However, the appointment number remains blocked in the database until the time of the appointment so that there can be no duplicate appointment numbers in the system.

```mermaid

graph TD

free
reserved
confirmed
queued
called
processing
pending
pickup
finished
missed
archived
deleted
anonymized["anonymized (not used)"]
blocked
conflict

db[(Database)]
cron[(Database)]
api((API))

db --find free slots for appointments--> free
free --checks if slot is still available--> reserved
reserved --appointment data is complete--> confirmed
reserved --appointment is not confirmed--> deleted
confirmed --workstation calls process--> called
api--client without appointment is added to the queue---->queued
queued --workstation calls process--> called
called --workstation confirms appearance of client--> processing
processing --workstation declares process for pickup--> pending
pending --workstation calls pending process--> pickup
processing --workstation finishes without statistics --> finished
processing --workstation finishes with statistics --> archived
called --workstation cannot confirm appearance of client--> missed
confirmed --appointment is cancelled--> deleted
deleted --slots are released, process.id is blocked--> blocked
confirmed --"does not fit into existing availability settings"--> conflict
deleted--slots are released-->free
blocked--nightly cronjob releases process.id-->cron 
finished-->blocked
archived-->blocked
pickup-->blocked
reserved--slots are released after reservation time-->free
missed --nightly cronjob release process.id--> cron
```