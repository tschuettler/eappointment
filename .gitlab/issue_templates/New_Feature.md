
<!--- 
Please write a short abstract for the feature.
The definition of the feature does not have to be perfect from the start, the definition of api routes or database changes can be skipped in the first iteration and can be specified as soon as the feature gets a timeline.
-->

Write a short abtract

### User stories

<!---
Typical roles:

 * client (customer who wants an appointment)
 * workstation user (user to call a client to an appointment)
 * scope/department administrator (user to administrate settings)
 * superuser (responsible for the system)
 * developer

-->

* As *role* I want to *have capability*, so that *I have this benefit*.

### View changes

<!---
Specify which views have to be changed or added.
Please consider, if a WCAG-Test ist required.
-->

#### Calendar for client

**URL-Example**: /admin/scope/123/

Add a button to ...

<!-- If no review is necessary, add a short explanation -->
**WCAG-Review required**: yes

### API changes

<!---
New or changed API routes should be defined as OpenAPI compatible YAML.
Method: Use a method like "get", "post", "delete"
Tags: The tagname should name involved entities
Route: API routes should start with the accessed entity, avoid actions in the name
Parameters: parameters should be defined in detail, especially the type to allow validation
Responses: At least a success and a failure status should exists.
-->

#### Route POST /entityname/{id}/relatedentity/

Write a description, what should the route do. Try to answer the following questions:

* Which kind of data is affected?
* Which kind of user access level is required to access this route?
* Which kind of errors can occur and why?
* Which security issues are related to this route and have to be tested by unit tests? (unpriviliged access, personal data leakage, performance issues for denial of service) 
* Which kind of fixture data is required to test the route?

```yaml
    "/entityname/{id}/relatedentity/":
        post:
            summary: write relatedentity for entity with given id
            tags:
                - entityname
            parameters:
                -   name: id
                    description: "id number"
                    in: path
                    required: true
                    type: integer
                -   name: token
                    description: "pre-shared token to identify requesting client"
                    type: string
                    in: query
                    required: true
                -   name: process
                    description: "process to confirm"
                    required: true
                    in: body
                    schema:
                        $ref: "schema/process.json"
                -   name: X-Authkey
                    required: true
                    description: "authentication key to identify user for testing access rights"
                    in: header
                    type: string
            responses:
                200:
                    description: "success"
                    schema:
                        type: object
                        properties:
                            meta:
                                $ref: "schema/metaresult.json"
                            data:
                                $ref: "schema/apikey.json"
                403:
                    description: "token not valid"
                    schema:
                        type: object
                        properties:
                            meta:
                                $ref: "schema/metaresult.json"

```

### DB changes

<!---
Please add needed migrations
Consider, if personal data is involved and documentation for GDPR has to be updated.
-->

|Table|Field|Change-Summary|
|---|---|---|
|scope|motd|Create TEXT: message of the day|

<!-- If no change is necessary, add a short explanation -->
**GDPR related**: no, because no personal information involved

### Further problems to check

<!---
Add notices for further problems to check during implementation.
-->
