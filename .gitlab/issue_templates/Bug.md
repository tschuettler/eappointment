<!--- 
Please write a short abstract for the feature.
The definition of the feature does not have to be perfect from the start, the definition of api routes or database changes can be skipped in the first iteration and can be specified as soon as the feature gets a timeline.
-->

## Preconditions
<!--- 
Please describe the conditions necessary to reproduce the bug. Include:

* Affected version
* Optional: Environment (productive enviroment or with test data)
* Optional: Used browser (if javascript is involved)
-->

## Test steps
<!--
Please describe the exact steps to reproduce the bug.
-->

## Actual behavior
<!--- 
Please provide error-messages or screenshots if necessary
-->

## Expected behavior
<!--- 
Please describe the desired behavior
-->


## Additional Links
<!--- 
You can add additional information here or you can link the issue in your ticket system.
-->

