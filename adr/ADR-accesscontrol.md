---
status: accepted
date: 2022-07-13
deciders: Mathias Fischer, Tobias Gassmann, Jan Ulrich Hartmann, Tobias Holler, Torsten Kunst, Waldemar Kowalenko, Tobias Wellens
---
# Advanced access control for useraccounts

## Context and Problem Statement

We need a more refined and flexible rights and roles concept. Right now the mapping is based on a hierarchical logic. Due to this logic certain requirements can't be fulfilled. 

<!-- This is an optional element. Feel free to remove. -->
## Decision Drivers

* Requirement to add a right to show the customer list (Kundenliste) (Berlin)
* Requirement for roles (functional admin: user management, configuration of locations; technical admin: opening hours, appointment management; clerk: appointment management) not compatible with the existing ones (München)

Low priority, workaround possible:

* Request for rights related to "scopes" and not to "department" (München)
* Request for different rights at different locations (Berlin)

## Considered Options

* OIDC/LDAP roles are used for rights
* Refined user rights management
* Editable Role based access control (RBAC)

## Decision Outcome

Chosen option: "Editable Role based access control (RBAC)", because it combines flexibility with an easy management of access rights.

The option OIDC/LDAP roles is dropped, as the new requirements expect a more flexible management of roles. The assignment of rights according to user account is dropped, as this makes the assignment of rights very time-consuming to administer.


## Pros and Cons of the Options

### OIDC/LDAP roles are used for rights

The rights are assigned via an external user management. This is done in the form of roles.

Pro

* Roles can be managed outside the application and central management is possible

Contra

* Problems with mapping
* Dependency on role concept of user management
* No refined or flexible right management possible


### Refined user rights management

The rights are assigned individually for each user account.

Pro

* Very flexible control of rights per user

Contra

* Time consuming administration of rights
* Greater susceptibility to errors in the administration process


### Editable Role based access control (RBAC)

Roles should be editable in the system. Individual roles are assigned to a user account

Pro

* High flexibility with less maintenance
* Less susceptibility to errors in the administration process

Contra

* More complicated to implement


