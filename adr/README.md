# Advanced Decision Records

We document decisions with advanced decision records. For more information, see https://adr.github.io/


Template: https://github.com/adr/madr/blob/main/template/adr-template.md

## Existing decision records

* [Advanced access control](ADR-accesscontrol.md)
